﻿using NUnit.Framework;
using RestSharp;
using System.Text.RegularExpressions;

namespace Comic.Test
{
    abstract public class ObjectTestsBase : RestClientBase
    {
        private IRestResponse _response;
        private string _responseContent;
        private readonly DataType? _dataType;
        private readonly string _objectName;
        private string _parameterName;
        private readonly string _queryParameters;
        string _graphqlQueryString;

        public ObjectTestsBase(string objectName, string queryParameters, string parameterName, DataType? datatype = null)
        {
            _objectName = objectName;
            _queryParameters = queryParameters;
            _parameterName = parameterName;
            _dataType = datatype;
            Setup();
        }

        public ObjectTestsBase(string objectName, string parameterName, DataType? datatype = null)
        {
            _objectName = objectName;
            _parameterName = parameterName;
            _dataType = datatype;
            Setup();
        }

        public ObjectTestsBase() { }

        //[SetUp]
        public void Setup()
        {
            BuildClient();
            BuildRequest();
            if (string.IsNullOrEmpty(_queryParameters))
                _graphqlQueryString = "{\"query\":\"{" + _objectName + "{" + _parameterName + "}}\",\"variables\":{}}";
            else
                _graphqlQueryString = "{\"query\":\"query{" + _objectName + "(" + _queryParameters + "){" + _parameterName + "}}\",\"variables\":{}}";
            _request.AddParameter("application/json", _graphqlQueryString, ParameterType.RequestBody);
            _response = _client.Execute(_request);
            if (_parameterName.Contains("{"))
                _parameterName = _parameterName.Replace("}", "").Split('{')[1];
            _responseContent = _response.Content;
        }

        [Test] public void Should_Return_200_Ok() => Assert.AreEqual(_response.StatusCode, System.Net.HttpStatusCode.OK, "Response was not set to Ok 200." + SummaryMessage());
        [Test] public void Should_Not_Be_Null() => Assert.AreNotEqual(_responseContent, null, "Response should not be null." + SummaryMessage());
        [Test] public void Should_Not_Contain_String_Error() => Assert.IsFalse(_responseContent.Contains("error"), "Response should not contain the string error. This is a BAD test but good for initial testing." + SummaryMessage());
        [Test] public void Should_Contain_ObjectName_As_String() => Assert.IsTrue(_responseContent.Contains(_objectName), $"Response should contain the name of the Object \"{_objectName}\"." + SummaryMessage());
        [Test] public void Should_Contain_ParameterName_As_String() => Assert.IsTrue(_responseContent.Contains(_parameterName), $"Response should contain the name of the ParameterName \"{_parameterName}\"." + SummaryMessage());

        [Test]
        public void Should_Contain_Correct_DataType_Response()
        {
            if (_dataType.HasValue)
                if (_dataType.Value == DataType.String)
                    Assert.IsTrue(Regex.IsMatch(_response.Content, _parameterName + "\":\"[^\"]+\""), $"Data should contain a string shape." + SummaryMessage());
                else if (_dataType.Value == DataType.Number)
                    Assert.IsTrue(Regex.IsMatch(_response.Content, _parameterName + "\":[0-9\\.]+"), $"Data should contain a number shape." + SummaryMessage());
                else if (_dataType.Value == DataType.DateTime)
                    Assert.IsTrue(Regex.IsMatch(_response.Content, _parameterName + "\":\"[TZ0-9: ]+\""), $"Data should contain a datetime shape." + SummaryMessage());
                else if (_dataType.Value == DataType.List)
                    Assert.IsTrue(Regex.IsMatch(_response.Content, _parameterName + "\":\\[\""), $"Data should contain a list shape." + SummaryMessage());
                else if (_dataType.Value == DataType.Object)
                    Assert.Fail();
        }

        private string SummaryMessage() => "\r\nRessponse content is " + _response.Content + "\r\nQuery is " + _graphqlQueryString;
    }
}