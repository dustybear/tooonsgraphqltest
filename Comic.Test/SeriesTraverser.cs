﻿using Newtonsoft.Json;
using NUnit.Framework;
using RestSharp;
using System;

namespace Comic.Test.SeriesTraverser
{
    public class SeriesTraverser : RestClientBase
    {
        TimeSpan TT;

        public SeriesTraverser()
        {
        }

        //[Test]
        public void TraversSeries()
        {
            //query{episodeList{title,images}}
            //query{seriesList{slug}}
            //query{singleSeries(slug:"a-fun-test-series"){episodes{slug, images}}}
            //query{singleSeries(slug:"a-fun-test-series\"){episodes{slug, images}}}
            //query { singleEpisode(slug: "eh-eh-eheh-eh-eh-e", seriesSlug:"a-fun-test-series") { images } }

            string allSeriesText = QueryBackend("seriesList", "slug", "");
            Model.Root _root = JsonConvert.DeserializeObject<Model.Root>(allSeriesText);
            Assert.IsNotNull(_root);
            Assert.IsNotNull(_root.data);
            Assert.IsNotNull(_root.data.seriesList);

            foreach (var series in _root.data.seriesList)
            {                
                string oneSeries = QueryBackend("singleSeries", "episodes{slug, images}", $"slug:\\\"{series.slug}\\\"");
                Model.Root _root2 = JsonConvert.DeserializeObject<Model.Root>(oneSeries);
                Assert.IsNotNull(_root2);
                Assert.IsNotNull(_root2.data);
                Assert.IsNotNull(_root2.data.singleSeries);
                Assert.IsNotNull(_root2.data.singleSeries.episodes);
                TestContext.Progress.WriteLine($"  {series.slug} ({TT.ToString()})");

                foreach (var episode in _root2.data.singleSeries.episodes)
                {
                    string oneEpisode = QueryBackend("singleEpisode", "images", $"slug:\\\"{episode.slug}\\\", seriesSlug:\\\"{series.slug}\\\"");
                    Model.Root _root3 = JsonConvert.DeserializeObject<Model.Root>(oneEpisode);
                    Assert.IsNotNull(_root3);
                    Assert.IsNotNull(_root3.data);
                    Assert.IsNotNull(_root3.data.singleEpisode);
                    TestContext.Progress.WriteLine($"    {episode.slug} ({TT.ToString()})");
                    
                    if (_root3.data.singleEpisode.images != null)
                        foreach (var image in _root3.data.singleEpisode.images)                        
                            TestContext.Progress.WriteLine("     " + image);                        
                }
            }
        }

        private string QueryBackend(string _objectName, string _parameterName, string _queryParameters)
        {
            BuildClient();
            BuildRequest();
            string _graphqlQueryString;
            if (string.IsNullOrEmpty(_queryParameters))
                _graphqlQueryString = "{\"query\":\"{" + _objectName + "{" + _parameterName + "}}\",\"variables\":{}}";
            else
                _graphqlQueryString = "{\"query\":\"query{" + _objectName + "(" + _queryParameters + "){" + _parameterName + "}}\",\"variables\":{}}";
            _request.AddParameter("application/json", _graphqlQueryString, ParameterType.RequestBody);
            DateTime T = System.DateTime.UtcNow;
            IRestResponse _response = _client.Execute(_request);
            TT = System.DateTime.UtcNow - T;
            var _responseContent = _response.Content;
            return _responseContent;
        }     
    }
}