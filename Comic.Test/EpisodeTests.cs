

using Comic.Test.Base;

namespace Comic.Test.Base
{
    public static class QueryStrings
    {
        public static string EpisodeQuery = "slug: \\\"episode-01\\\", seriesSlug: \\\"midnights-war\\\"";
        public static string SeriesQuery = "slug: \\\"a-fun-test-series\\\"";
        public static string UserQuery = "uuid: \\\"XXXXX\\\", username: \\\"XXXXXXXXXX\\\"";
    }
}
//namespace Comic.Test.Users
//{
//    public class TestUsersParameterFirstname : ObjectTestsBase { public TestUsersParameterFirstname() : base("users", "firstname", DataType.String) { } }
//    public class TestUsersParameterLastname : ObjectTestsBase { public TestUsersParameterLastname() : base("users", "lastname", DataType.String) { } }
//    public class TestUsersParameterUsername : ObjectTestsBase { public TestUsersParameterUsername() : base("users", "username", DataType.String) { } }
//    public class TestUsersParameterEmail : ObjectTestsBase { public TestUsersParameterEmail() : base("users", "email", DataType.String) { } }
//    public class TestUsersParameterCreatedAt : ObjectTestsBase { public TestUsersParameterCreatedAt() : base("users", "createdAt", DataType.DateTime) { } }
//    public class TestUsersParameterUpdatedAt : ObjectTestsBase { public TestUsersParameterUpdatedAt() : base("users", "updatedAt", DataType.DateTime) { } }
//}
//namespace Comic.Test.User
//{
//    public class TestUserParameterName : ObjectTestsBase { public TestUserParameterName() : base("user", QueryStrings.UserQuery, "name", DataType.String) { } }
//    public class TestUserParameterUsername : ObjectTestsBase { public TestUserParameterUsername() : base("user", QueryStrings.UserQuery, "username", DataType.String) { } }
//    public class TestUserParameterEmail : ObjectTestsBase { public TestUserParameterEmail() : base("user", QueryStrings.UserQuery, "email", DataType.String) { } }
//    public class TestUserParameterCreatedAt : ObjectTestsBase { public TestUserParameterCreatedAt() : base("user", QueryStrings.UserQuery, "createdAt", DataType.DateTime) { } }
//    public class TestUserParameterUpdatedAt : ObjectTestsBase { public TestUserParameterUpdatedAt() : base("user", QueryStrings.UserQuery, "updatedAt", DataType.DateTime) { } }
//}
//namespace Comic.Test.SeriesByUsername
//{
//    public class TestSerieByUsernameParameterTitle : ObjectTestsBase { public TestSerieByUsernameParameterTitle() : base("seriesByUsername", QueryStrings.SeriesQuery, "title", DataType.String) { } }
//    public class TestSerieByUsernameParameterSlug : ObjectTestsBase { public TestSerieByUsernameParameterSlug() : base("seriesByUsername", QueryStrings.SeriesQuery, "slug", DataType.String) { } }
//    public class TestSerieByUsernameParameterPublisher : ObjectTestsBase { public TestSerieByUsernameParameterPublisher() : base("seriesByUsername", QueryStrings.SeriesQuery, "publisher", DataType.String) { } }
//    public class TestSerieByUsernameParameterSummary : ObjectTestsBase { public TestSerieByUsernameParameterSummary() : base("seriesByUsername", QueryStrings.SeriesQuery, "summary", DataType.String) { } }
//    public class TestSerieByUsernameParameterNumEpisodes : ObjectTestsBase { public TestSerieByUsernameParameterNumEpisodes() : base("seriesByUsername", QueryStrings.SeriesQuery, "numEpisodes", DataType.String) { } }
//    public class TestSerieByUsernameParameterExplicit : ObjectTestsBase { public TestSerieByUsernameParameterExplicit() : base("seriesByUsername", QueryStrings.SeriesQuery, "explicit", DataType.Boolean) { } }
//    public class TestSerieByUsernameParameterSubscribers : ObjectTestsBase { public TestSerieByUsernameParameterSubscribers() : base("seriesByUsername", QueryStrings.SeriesQuery, "subscribers", DataType.Int) { } }
//    public class TestSerieByUsernameParameterAvgRating : ObjectTestsBase { public TestSerieByUsernameParameterAvgRating() : base("seriesByUsername", QueryStrings.SeriesQuery, "avgRating", DataType.Float) { } }
//    public class TestSerieByUsernameParameterReleaseDay : ObjectTestsBase { public TestSerieByUsernameParameterReleaseDay() : base("seriesByUsername", QueryStrings.SeriesQuery, "releaseDay", DataType.String) { } }
//    public class TestSerieByUsernameParameterCoverArtWorkOrigSrc : ObjectTestsBase { public TestSerieByUsernameParameterCoverArtWorkOrigSrc() : base("seriesByUsername", QueryStrings.SeriesQuery, "cover{origSrc}", DataType.String) { } }
//    public class TestSerieByUsernameParameterCoverArtWorkAltText : ObjectTestsBase { public TestSerieByUsernameParameterCoverArtWorkAltText() : base("seriesByUsername", QueryStrings.SeriesQuery, "cover{altText}", DataType.String) { } }
//    public class TestSerieByUsernameParameterGenre : ObjectTestsBase { public TestSerieByUsernameParameterGenre() : base("seriesByUsername", QueryStrings.SeriesQuery, "genre", DataType.List) { } }
//    public class TestSerieByUsernameParameterEpisodes : ObjectTestsBase { public TestSerieByUsernameParameterEpisodes() : base("seriesByUsername", QueryStrings.SeriesQuery, "episodeList", DataType.List) { } }
//    public class TestSerieByUsernameParameterGenderTargetsName : ObjectTestsBase { public TestSerieByUsernameParameterGenderTargetsName() : base("seriesByUsername", QueryStrings.SeriesQuery, "genderTargets{name}", DataType.String) { } }
//    public class TestSerieByUsernameParameterGenderTargetsSlug : ObjectTestsBase { public TestSerieByUsernameParameterGenderTargetsSlug() : base("seriesByUsername", QueryStrings.SeriesQuery, "genderTargets{slug}", DataType.String) { } }
//    public class TestSerieByUsernameParameterAgeGroups : ObjectTestsBase { public TestSerieByUsernameParameterAgeGroups() : base("seriesByUsername", QueryStrings.SeriesQuery, "ageGroups", DataType.List) { } }
//    public class TestSerieByUsernameParameterPublishedAt : ObjectTestsBase { public TestSerieByUsernameParameterPublishedAt() : base("seriesByUsername", QueryStrings.SeriesQuery, "publishedAt", DataType.DateTime) { } }
//    public class TestSerieByUsernameParameterCreatedAt : ObjectTestsBase { public TestSerieByUsernameParameterCreatedAt() : base("seriesByUsername", QueryStrings.SeriesQuery, "createdAt", DataType.DateTime) { } }
//    public class TestSerieByUsernameParameterUpdatedAt : ObjectTestsBase { public TestSerieByUsernameParameterUpdatedAt() : base("seriesByUsername", QueryStrings.SeriesQuery, "updatedAt", DataType.DateTime) { } }
//}
namespace Comic.Test.Series
{
    public class TestSeriesParametertitle : ObjectTestsBase { public TestSeriesParametertitle() : base("seriesList", "title", DataType.String) { } }
    public class TestSeriesParameterslug : ObjectTestsBase { public TestSeriesParameterslug() : base("seriesList", "slug", DataType.String) { } }
    public class TestSeriesParametergenre_name : ObjectTestsBase { public TestSeriesParametergenre_name() : base("seriesList", "genre{name}", DataType.String) { } }
    public class TestSeriesParametergenre_slug : ObjectTestsBase { public TestSeriesParametergenre_slug() : base("seriesList", "genre{slug}", DataType.String) { } }
    public class TestSeriesParametersummary : ObjectTestsBase { public TestSeriesParametersummary() : base("seriesList", "summary", DataType.String) { } }
    public class TestSeriesParameterrating : ObjectTestsBase { public TestSeriesParameterrating() : base("seriesList", "rating", DataType.String) { } }
    public class TestSeriesParameterreleaseDay : ObjectTestsBase { public TestSeriesParameterreleaseDay() : base("seriesList", "releaseDay", DataType.String) { } }
    public class TestSeriesParametergenderTargets_name : ObjectTestsBase { public TestSeriesParametergenderTargets_name() : base("seriesList", "genderTargets{name}", DataType.String) { } }
    public class TestSeriesParametergenderTargets_slug : ObjectTestsBase { public TestSeriesParametergenderTargets_slug() : base("seriesList", "genderTargets{slug}", DataType.String) { } }
    //public class TestSeriesParameternumEpisodes : ObjectTestsBase { public TestSeriesParameternumEpisodes() : base("seriesList", "numEpisodes", DataType.String) { } }
    public class TestSeriesParametersubscribers : ObjectTestsBase { public TestSeriesParametersubscribers() : base("seriesList", "subscribers", DataType.Int) { } }
    public class TestSeriesParameteravgRating : ObjectTestsBase { public TestSeriesParameteravgRating() : base("seriesList", "avgRating", DataType.Float) { } }
    public class TestSeriesParameterageGroups_name : ObjectTestsBase { public TestSeriesParameterageGroups_name() : base("seriesList", "ageGroups{name}", DataType.String) { } }
    public class TestSeriesParameterageGroups_slug : ObjectTestsBase { public TestSeriesParameterageGroups_slug() : base("seriesList", "ageGroups{slug}", DataType.String) { } }
    //public class TestSeriesParameterpublishedAt : ObjectTestsBase { public TestSeriesParameterpublishedAt() : base("seriesList", "publishedAt", DataType.DateTime) { } }
   // public class TestSeriesParametercreatedAt : ObjectTestsBase { public TestSeriesParametercreatedAt() : base("seriesList", "createdAt", DataType.DateTime) { } }
   // public class TestSeriesParameterhasCoverPhoto : ObjectTestsBase { public TestSeriesParameterhasCoverPhoto() : base("seriesList", "hasCoverPhoto", DataType.Boolean) { } }
    public class TestSeriesParameterhasHeaderPhoto : ObjectTestsBase { public TestSeriesParameterhasHeaderPhoto() : base("seriesList", "hasHeaderPhoto", DataType.Boolean) { } }
}
namespace Comic.Test.SingleSeries
{
    public class TestSeriesParametertitle : ObjectTestsBase { public TestSeriesParametertitle() : base("singleSeries", QueryStrings.SeriesQuery, "title", DataType.String) { } }
    public class TestSeriesParameterslug : ObjectTestsBase { public TestSeriesParameterslug() : base("singleSeries", QueryStrings.SeriesQuery, "slug", DataType.String) { } }
    public class TestSeriesParametergenre_name : ObjectTestsBase { public TestSeriesParametergenre_name() : base("singleSeries", QueryStrings.SeriesQuery, "genre{name}", DataType.String) { } }
    public class TestSeriesParametergenre_slug : ObjectTestsBase { public TestSeriesParametergenre_slug() : base("singleSeries", QueryStrings.SeriesQuery, "genre{slug}", DataType.String) { } }
    public class TestSeriesParametersummary : ObjectTestsBase { public TestSeriesParametersummary() : base("singleSeries", QueryStrings.SeriesQuery, "summary", DataType.String) { } }
    public class TestSeriesParameterrating : ObjectTestsBase { public TestSeriesParameterrating() : base("singleSeries", QueryStrings.SeriesQuery, "rating", DataType.String) { } }
    public class TestSeriesParameterreleaseDay : ObjectTestsBase { public TestSeriesParameterreleaseDay() : base("singleSeries", QueryStrings.SeriesQuery, "releaseDay", DataType.String) { } }
    public class TestSeriesParametergenderTargets_name : ObjectTestsBase { public TestSeriesParametergenderTargets_name() : base("singleSeries", QueryStrings.SeriesQuery, "genderTargets{name}", DataType.String) { } }
    public class TestSeriesParametergenderTargets_slug : ObjectTestsBase { public TestSeriesParametergenderTargets_slug() : base("singleSeries", QueryStrings.SeriesQuery, "genderTargets{slug}", DataType.String) { } }
    //public class TestSeriesParameternumEpisodes : ObjectTestsBase { public TestSeriesParameternumEpisodes() : base("singleSeries", QueryStrings.SeriesQuery, "numEpisodes", DataType.String) { } }
    public class TestSeriesParametersubscribers : ObjectTestsBase { public TestSeriesParametersubscribers() : base("singleSeries", QueryStrings.SeriesQuery, "subscribers", DataType.Int) { } }
    public class TestSeriesParameteravgRating : ObjectTestsBase { public TestSeriesParameteravgRating() : base("singleSeries", QueryStrings.SeriesQuery, "avgRating", DataType.Float) { } }
    public class TestSeriesParameterageGroups_name : ObjectTestsBase { public TestSeriesParameterageGroups_name() : base("singleSeries", QueryStrings.SeriesQuery, "ageGroups{name}", DataType.String) { } }
    public class TestSeriesParameterageGroups_slug : ObjectTestsBase { public TestSeriesParameterageGroups_slug() : base("singleSeries", QueryStrings.SeriesQuery, "ageGroups{slug}", DataType.String) { } }
    //public class TestSeriesParameterpublishedAt : ObjectTestsBase { public TestSeriesParameterpublishedAt() : base("singleSeries", QueryStrings.SeriesQuery, "publishedAt", DataType.DateTime) { } }
    //public class TestSeriesParametercreatedAt : ObjectTestsBase { public TestSeriesParametercreatedAt() : base("singleSeries", QueryStrings.SeriesQuery, "createdAt", DataType.DateTime) { } }
    //public class TestSeriesParameterupdatedAt : ObjectTestsBase { public TestSeriesParameterupdatedAt() : base("singleSeries", QueryStrings.SeriesQuery, "updatedAt", DataType.DateTime) { } }
    public class TestSeriesParameterhasCoverPhoto : ObjectTestsBase { public TestSeriesParameterhasCoverPhoto() : base("singleSeries", QueryStrings.SeriesQuery, "hasCoverPhoto", DataType.Boolean) { } }
    public class TestSeriesParameterhasHeaderPhoto : ObjectTestsBase { public TestSeriesParameterhasHeaderPhoto() : base("singleSeries", QueryStrings.SeriesQuery, "hasHeaderPhoto", DataType.Boolean) { } }
}
namespace Comic.Test.Episodes
{
    public class TestEpisodesParameterslug : ObjectTestsBase { public TestEpisodesParameterslug() : base("episodeList", "slug", DataType.String) { } }
    public class TestEpisodesParametertitle : ObjectTestsBase { public TestEpisodesParametertitle() : base("episodeList", "title", DataType.String) { } }
    public class TestEpisodesParametergenre : ObjectTestsBase { public TestEpisodesParametergenre() : base("episodeList", "genre", DataType.Int) { } }
    public class TestEpisodesParameternumInSeries : ObjectTestsBase { public TestEpisodesParameternumInSeries() : base("episodeList", "numInSeries", DataType.Int) { } }
    public class TestEpisodesParameterapprovalStatus : ObjectTestsBase { public TestEpisodesParameterapprovalStatus() : base("episodeList", "approvalStatus", DataType.String) { } }
    public class TestEpisodesParametermediaDir : ObjectTestsBase { public TestEpisodesParametermediaDir() : base("episodeList", "mediaDir", DataType.String) { } }
    //public class TestEpisodesParameterpublicationDate : ObjectTestsBase { public TestEpisodesParameterpublicationDate() : base("episodeList", "publicationDate", DataType.DateTime) { } }
    public class TestEpisodesParameterisPublished : ObjectTestsBase { public TestEpisodesParameterisPublished() : base("episodeList", "isPublished", DataType.String) { } }
    //public class TestEpisodesParameterbackgroundColor : ObjectTestsBase { public TestEpisodesParameterbackgroundColor() : base("episodeList", "backgroundColor", DataType.String) { } }
    public class TestEpisodesParameterimages : ObjectTestsBase { public TestEpisodesParameterimages() : base("episodeList", "images", DataType.List) { } }
    //public class TestEpisodesParametercreatedAt : ObjectTestsBase { public TestEpisodesParametercreatedAt() : base("episodeList", "createdAt", DataType.DateTime) { } }
    //public class TestEpisodesParameterupdatedAt : ObjectTestsBase { public TestEpisodesParameterupdatedAt() : base("episodeList", "updatedAt", DataType.DateTime) { } }
    public class TestEpisodesParameterviews : ObjectTestsBase { public TestEpisodesParameterviews() : base("episodeList", "views", DataType.Int) { } }
    public class TestEpisodesParameterlikes : ObjectTestsBase { public TestEpisodesParameterlikes() : base("episodeList", "likes", DataType.Int) { } }
}
namespace Comic.Test.Episode
{
    public class TestEpisodeParameterslug : ObjectTestsBase { public TestEpisodeParameterslug() : base("singleEpisode", QueryStrings.EpisodeQuery, "slug", DataType.String) { } }
    public class TestEpisodeParametertitle : ObjectTestsBase { public TestEpisodeParametertitle() : base("singleEpisode", QueryStrings.EpisodeQuery, "title", DataType.String) { } }
    public class TestEpisodeParametergenre : ObjectTestsBase { public TestEpisodeParametergenre() : base("singleEpisode", QueryStrings.EpisodeQuery, "genre", DataType.Int) { } }
    public class TestEpisodeParameternumInSeries : ObjectTestsBase { public TestEpisodeParameternumInSeries() : base("singleEpisode", QueryStrings.EpisodeQuery, "numInSeries", DataType.Int) { } }
    public class TestEpisodeParameterapprovalStatus : ObjectTestsBase { public TestEpisodeParameterapprovalStatus() : base("singleEpisode", QueryStrings.EpisodeQuery, "approvalStatus", DataType.String) { } }
    public class TestEpisodeParametermediaDir : ObjectTestsBase { public TestEpisodeParametermediaDir() : base("singleEpisode", QueryStrings.EpisodeQuery, "mediaDir", DataType.String) { } }
   // public class TestEpisodeParameterpublicationDate : ObjectTestsBase { public TestEpisodeParameterpublicationDate() : base("singleEpisode", QueryStrings.EpisodeQuery, "publicationDate", DataType.DateTime) { } }
    public class TestEpisodeParameterisPublished : ObjectTestsBase { public TestEpisodeParameterisPublished() : base("singleEpisode", QueryStrings.EpisodeQuery, "isPublished", DataType.String) { } }
    //public class TestEpisodeParameterbackgroundColor : ObjectTestsBase { public TestEpisodeParameterbackgroundColor() : base("singleEpisode", QueryStrings.EpisodeQuery, "backgroundColor", DataType.String) { } }
    public class TestEpisodeParameterimages : ObjectTestsBase { public TestEpisodeParameterimages() : base("singleEpisode", QueryStrings.EpisodeQuery, "images", DataType.List) { } }
    //public class TestEpisodeParametercreatedAt : ObjectTestsBase { public TestEpisodeParametercreatedAt() : base("singleEpisode", QueryStrings.EpisodeQuery, "createdAt", DataType.DateTime) { } }
   // public class TestEpisodeParameterupdatedAt : ObjectTestsBase { public TestEpisodeParameterupdatedAt() : base("singleEpisode", QueryStrings.EpisodeQuery, "updatedAt", DataType.DateTime) { } }
    public class TestEpisodeParameterviews : ObjectTestsBase { public TestEpisodeParameterviews() : base("singleEpisode", QueryStrings.EpisodeQuery, "views", DataType.Int) { } }
    public class TestEpisodeParameterlikes : ObjectTestsBase { public TestEpisodeParameterlikes() : base("singleEpisode", QueryStrings.EpisodeQuery, "likes", DataType.Int) { } }
}