﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Comic.Test.Model
{
    public class SingleEpisode
    {
        public string title { get; set; }
        public string slug { get; set; }
        public int likes { get; set; }
        public int views { get; set; }
        public List<string> images { get; set; }
    }

    public class Series
    {
        public string slug { get; set; }
        public List<SingleEpisode> episodes { get; set; }
    }

    public class Data
    {
        public List<Series> seriesList { get; set; }
        public Series singleSeries { get; set; }
        public SingleEpisode singleEpisode { get; set; }
    }

    public class Root
    {
        public Data data { get; set; }
    }
}
