﻿using RestSharp;

namespace Comic.Test
{
    abstract public class RestClientBase
    {
        protected RestClient _client;
        protected RestRequest _request;
        protected void BuildClient()
        {
            _client = new RestClient("http://dev.arkhaven.com/graphql");
            _client.Timeout = -1;
        }
        protected void BuildRequest()
        {
            _request = new RestRequest(Method.POST);
            _request.AddHeader("Authorization", "TaK62.C_+2@iKX*+2_.3&,iLJ-,A");
            _request.AddHeader("Content-Type", "application/json");
        }
    }
}