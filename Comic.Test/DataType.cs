﻿namespace Comic.Test
{
    public enum DataType
    { 
        String,
        Number,
        DateTime,
        Boolean, 
        Int,
        Float,
        List,
        Object
    }
}