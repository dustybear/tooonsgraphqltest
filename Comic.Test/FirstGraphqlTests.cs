﻿using NUnit.Framework;
using RestSharp;

namespace Comic.Test
{
    public class FirstGraphqlTests
    {

        [Test]
        public void FirstCallShouldNotBeNull()
        {
            var client = new RestClient("http://dev.arkhaven.com/graphql");
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("Authorization", "TaK62.C_+2@iKX*+2_.3&,iLJ-,A");
            request.AddParameter("application/json", "{\"query\":\"query{episodes{title}}\",\"variables\":{}}", ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            Assert.AreNotEqual(response.Content, null);
        }
    }
}