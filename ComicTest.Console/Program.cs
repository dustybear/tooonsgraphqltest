﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Comic.Test;
using Comic.Test.Model;
using Newtonsoft.Json;
using RestSharp;

namespace ComicTest.Console1
{
    class Program
    {
        static void Main(string[] args)
        {
            int count = 30;
            if (args.Length == 1)
                count = Convert.ToInt32(args[0]);

            SeriesTraverser seriesTraverser = new SeriesTraverser();

            int start = seriesTraverser.QueryEpisodeViews("midnights-war", "episode-01");
            DateTime T = DateTime.UtcNow;
                        
            seriesTraverser.Caller(count).Wait();

            TimeSpan TT = DateTime.UtcNow - T;
            int end = seriesTraverser.QueryEpisodeViews("midnights-war", "episode-01");

            Console.WriteLine("time taken (sec) " + TT.TotalSeconds);
            Console.WriteLine("time taken per request (sec) " + TT.TotalSeconds / (count * 10));
            Console.WriteLine("increment proposed " + (count * 10));
            Console.WriteLine("increment acheived " + (end - start));            
        }
    }

    public class SeriesTraverser : RestClientBase
    {
        public SeriesTraverser()
        {
            BuildClient();
        }

        public async Task Caller(int count) {
            List<string> pricingZones = new List<string> { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j" };
            for (int i = 0; i < count; i++)
            {                
                IEnumerable<Task> tasks = pricingZones.Select(pricingZoneItem =>
                {
                    return IncrementEpisodeViews("d56ea1d5-cc0d-4a22-92e9-128c03a141a8", "8cd7eddd-c87f-487a-85ad-a35b1eb90de3");
                });
                await Task.WhenAll(tasks);
                Console.Write("+");
            }
        }

        public async Task<string>  IncrementEpisodeViews(string seriesId, string episodeId)
        {
            Console.Write(".");
            BuildRequest();
            string _graphqlQueryString = "{\"operationName\":null,\"variables\":{\"input\":{\"series_uuid\":\"" + seriesId + "\",\"episode_uuid\":\"" + episodeId + "\"}},\"query\":\"mutation($input:CountsInput!){incrementEpisodeViews(input:$input){insertId}}\"}";
            _request.AddParameter("application/json", _graphqlQueryString, ParameterType.RequestBody);
            IRestResponse _response = _client.Execute(_request);
            var _responseContent = _response.Content;
            return _responseContent;
        }

        public int QueryEpisodeViews(string seriesSlug, string episodeSlug)
        {
            BuildRequest();
            string _graphqlQueryString = "{\"query\":\"query{singleEpisode(slug:\\\"" + episodeSlug + "\\\",seriesSlug:\\\"" + seriesSlug + "\\\"){views}}\"}";
            _request.AddOrUpdateParameter("application/json", _graphqlQueryString, ParameterType.RequestBody);
            IRestResponse _response = _client.Execute(_request);
            var _responseContent = _response.Content;
            Root _root3 = JsonConvert.DeserializeObject<Root>(_responseContent);
            return _root3.data.singleEpisode.views;
        }
    }
}

//public void TraversSeries()
//{
//    //query{episodeList{title,images}}
//    //query{seriesList{slug}}
//    //query{singleSeries(slug:"a-fun-test-series"){episodes{slug, images}}}
//    //query{singleSeries(slug:"a-fun-test-series\"){episodes{slug, images}}}
//    //query { singleEpisode(slug: "eh-eh-eheh-eh-eh-e", seriesSlug:"a-fun-test-series") { images } }

//    string allSeriesText = QueryBackend("seriesList", "slug", "");
//    Root _root = JsonConvert.DeserializeObject<Root>(allSeriesText);
//    foreach (var series in _root.data.seriesList)
//    {
//        string oneSeries = QueryBackend("singleSeries", "episodes{slug, images}", $"slug:\\\"{series.slug}\\\"");
//        Root _root2 = JsonConvert.DeserializeObject<Root>(oneSeries);
//        Console.WriteLine($"  {series.slug}");
//        foreach (var episode in _root2.data.singleSeries.episodes)
//        {
//            string oneEpisode = QueryBackend("singleEpisode", "images", $"slug:\\\"{episode.slug}\\\", seriesSlug:\\\"{series.slug}\\\"");
//            Root _root3 = JsonConvert.DeserializeObject<Root>(oneEpisode);
//            Console.WriteLine($"    {episode.slug}");
//            if (_root3.data.singleEpisode.images != null)
//                foreach (var image in _root3.data.singleEpisode.images)
//                    Console.WriteLine("     " + image);
//        }
//    }
//}

////List<string> pricingZones = new List<string>(PricingService.ZonePostCodeMap.Keys);
////IEnumerable<Task> tasks = pricingZones.Select(pricingZoneItem =>
////{
////    return GetDriveAwayPriceByZone(rootVehicle, retailPricing, pricingZoneFilter, driveAwayResponseResults, pricingZoneItem);
////});
////await Task.WhenAll(tasks);

//public string QueryBackend(string _objectName, string _parameterName, string _queryParameters)
//{
//    string _graphqlQueryString;
//    if (_queryParameters == "mutationIncrementor")
//        _graphqlQueryString = "{\"query\":\"{mutation($input: CountsInput!) {incrementEpisodeViews(input: $input) {insertId}}}\",\"variables\":{\"input\": {\"series_uuid\": \"d56ea1d5-cc0d-4a22-92e9-128c03a141a8\",\"episode_uuid\":\"8cd7eddd-c87f-487a-85ad-a35b1eb90de3\"}}}}";
//    else if (string.IsNullOrEmpty(_queryParameters))
//        _graphqlQueryString = "{\"query\":\"{" + _objectName + "{" + _parameterName + "}}\",\"variables\":{}}";
//    else
//        _graphqlQueryString = "{\"query\":\"query{" + _objectName + "(" + _queryParameters + "){" + _parameterName + "}}\",\"variables\":{}}";

//    _request.AddOrUpdateParameter("application/json", _graphqlQueryString, ParameterType.RequestBody);
//    IRestResponse _response = _client.Execute(_request);
//    var _responseContent = _response.Content;
//    return _responseContent;
//}
